/* This is the complete MAC program for doing: encryption and CRC.

   The steps are:
   1. The CRC of the frame is calculated.
   2. The frame is encrypted.
   3. The CRC from Step1. is appended to the encrypted frame from Step2.
   4. The CRC of this whole payload is calculated and appended to the frame.
*/

#include "crc_table.h"

// Function declarations

void sbox_setup(unsigned char seed[]);
unsigned char get_wep_byte(void);
void wep_encrypt(int plaintext_len, unsigned char plaintext[], unsigned char ciphertext[], unsigned char seed[]);
unsigned long get_crc(int no_of_bytes, unsigned char data[]);
extern void WriteDebuggerWindow(unsigned long*, unsigned int);

// Global Variable declarations

static unsigned char sbox[256];
static unsigned char wep_index1;
static unsigned char wep_index2;

void sbox_setup(unsigned char seed[])
{
    unsigned char index, temp;
    short counter;

    // Initialize index variables
    index = 0;
    wep_index1 = 0;
    wep_index2 = 0;

    for (counter = 0; counter < 256; counter++)
	sbox[counter] = (unsigned char)counter;

    for (counter = 0; counter < 256; counter++) {
	// Calculate index
	index = (index + sbox[counter] + seed[counter % 8]) % 256;

	// Swap bytes
	temp = sbox[counter];
	sbox[counter] = sbox[index];
	sbox[index] = temp;
    }
}

unsigned char get_wep_byte(void)
{
    unsigned char index, temp;

    wep_index1 = (wep_index1 + 1) % 256;
    wep_index2 = (wep_index2 + sbox[wep_index1]) % 256;
    temp = sbox[wep_index1];
    sbox[wep_index1] = sbox[wep_index2];
    sbox[wep_index2] = temp;
    index = (sbox[wep_index1] + sbox[wep_index2]) % 256;

    return sbox[index];
}

void wep_encrypt(int plaintext_len, unsigned char plaintext[], unsigned char ciphertext[], unsigned char seed[])
{
    int i;

    sbox_setup(seed);

    for (i = 0; i < plaintext_len; i++) {
	ciphertext[i] = plaintext[i] ^ get_wep_byte();
    }
}

unsigned long get_crc(int no_of_bytes, unsigned char data[])
{
    unsigned long crc_temp = 0xFFFFFFFF;
    int i, index;

    index = ((int)(crc_temp >> 24)) & 0xFF;
    crc_temp = (crc_temp << 8) ^ crc_table[index];

    for (i = 0; i < no_of_bytes; i++) {
	index = ((int)(crc_temp >> 24) ^ data[i]) & 0xFF;
	crc_temp = (crc_temp << 8) ^ crc_table[index];
    }

    crc_temp = crc_temp ^ 0xFFFFFFFF;

    return crc_temp;
}

int TestMain()
{
// ensure that no functions using semihosting SWIs are linked in from the C library
#pragma import(__use_no_semihosting_swi)

    // Initialize the plaintext data
    unsigned char plaintext[48] = {
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
	0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80,
	0x00, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
	0x00, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89,
	0x28, 0x47, 0x37, 0x86, 0x31, 0x7F, 0x4D, 0x86
    };

    // Declare the ciphertext array. It has 8 more bytes for 4+4 bytes of CRC data
    unsigned char ciphertext[56];
    unsigned long no_of_bytes = 48;
    unsigned long crc;
    int i;
    int j; // Added by kenyon for project 4.
    unsigned char seed[8];

    // These names correspond to the ones used in wep_encrypt.v.
    // Added by kenyon for project 4.
    unsigned long plaintext_frame_start		= 0x01000100;
    unsigned long ciphertext_frame_start	= 0x01000200;
    unsigned long start_encrypt			= 0x01000004;
    unsigned long plain_addr			= 0x01000008;
    unsigned long cipher_addr			= 0x01000010;
    unsigned long frame_size			= 0x0100000C;
    unsigned long seed_lsw			= 0x01000014;
    //unsigned long seed_msw			= 0x01000018;
    unsigned long done				= 0x0100001C;

    // Added by kenyon for project 4.
    unsigned long* mem_addr;
    mem_addr = (unsigned long*)plaintext_frame_start;

    // The 5 MSBs of the seed do not change with the frame
    // The 3 LSBs change with each new frame (part of specification)
    seed[3] = 0x67;
    seed[4] = 0x89;
    seed[5] = 0xAB;
    seed[6] = 0xCD;
    seed[7] = 0xEF;

    for (i = 0; i < 1; i++) {
	seed[0] = 0x01;
	seed[1] = 0x23;
	seed[2] = 0x45;

	// Compute the CRC of the plaintext
	crc = get_crc(no_of_bytes, plaintext);

	// Start of project 4 code by kenyon.

	// Write the plaintext frame to DPSRAM (pld slave), starting at address
	// plaintext_frame_start.
	for (j = 0; j < no_of_bytes; j += 4, ++mem_addr) {
	    *mem_addr = 0;
	    *mem_addr |= (unsigned long)(plaintext[j + 0] << 0);
	    *mem_addr |= (unsigned long)(plaintext[j + 1] << 8);
	    *mem_addr |= (unsigned long)(plaintext[j + 2] << 16);
	    *mem_addr |= (unsigned long)(plaintext[j + 3] << 24);
	}

	// Write the starting address of the plaintext frame
	// (plaintext_frame_start) to the address corresponding to plain_addr.
	mem_addr = (unsigned long*)plain_addr;
	*mem_addr = plaintext_frame_start;

	// Write the size of the plaintext frame to the address corresponding
	// to frame_size.
	mem_addr = (unsigned long*)frame_size;
	*mem_addr = no_of_bytes;

	// Write the starting address of the ciphertext frame
	// (ciphertext_frame_start) to the address corresponding to
	// cipher_addr.
	mem_addr = (unsigned long*)cipher_addr;
	*mem_addr = ciphertext_frame_start;

	// Write the lsw of the seed to the address corresponding to seed_lsw.
	// Write the msw of the seed to the address corresponding to seed_msw.
	mem_addr = (unsigned long*)seed_lsw;
	for (j = 0; j < 8; j += 4, ++mem_addr) {
	    *mem_addr = 0;
	    *mem_addr |= (unsigned long)(seed[j + 0] << 0);
	    *mem_addr |= (unsigned long)(seed[j + 1] << 8);
	    *mem_addr |= (unsigned long)(seed[j + 2] << 16);
	    *mem_addr |= (unsigned long)(seed[j + 3] << 24);
	}

	// Write a "1" to the address corresponding to start_encrypt. After
	// this, remember to deassert the start signal, by writing a "0" to the
	// same location.
	mem_addr = (unsigned long*)start_encrypt;
	*mem_addr = 1;
	*mem_addr = 0;

	// Poll "done" register.
	mem_addr = (unsigned long*)done;
	while (*mem_addr != 1) {
	    // Busy waiting.
	}

	// Read ciphertext from DPSRAM.
	mem_addr = (unsigned long*)ciphertext_frame_start;
	for (j = 0; j < no_of_bytes; j += 4, ++mem_addr) {
	    ciphertext[j + 0] = (unsigned char)(*mem_addr >> 0);
	    ciphertext[j + 1] = (unsigned char)(*mem_addr >> 8);
	    ciphertext[j + 2] = (unsigned char)(*mem_addr >> 16);
	    ciphertext[j + 3] = (unsigned char)(*mem_addr >> 24);
	}

	// End of project 4 code by kenyon.

	// Append the CRC to the ciphertext
	ciphertext[no_of_bytes]   = (unsigned char)(crc >> 24);
	ciphertext[no_of_bytes+1] = (unsigned char)(crc >> 16);
	ciphertext[no_of_bytes+2] = (unsigned char)(crc >> 8);
	ciphertext[no_of_bytes+3] = (unsigned char)(crc);

	// Compute the CRC of the whole payload
	crc = get_crc(no_of_bytes+4, ciphertext);

	// Append the CRC to the payload
	ciphertext[no_of_bytes+4]   = (unsigned char)(crc >> 24);
	ciphertext[no_of_bytes+4+1] = (unsigned char)(crc >> 16);
	ciphertext[no_of_bytes+4+2] = (unsigned char)(crc >> 8);
	ciphertext[no_of_bytes+4+3] = (unsigned char)(crc);

	// Display the final frame which would be transmitted
	// Cannot have printf statements in code
	//fprintf(stdout, "%x\n", ciphertext[j]);

	// Display is handled by XRAY debugger
	// You'll have to single step through the code for this function
	// There is some bug in XRAY which causes it to crash if you
	// just run through this.
	// Set a breakpoint to just before this function and then single step
	// through the code to display the final frame which would be
	// transmitted
	// Alternately, you can also view the appropriate memory location
	// of ciphertext to see the final data.

	WriteDebuggerWindow((unsigned long*)ciphertext, no_of_bytes+8);
    }

    return 1;
}
